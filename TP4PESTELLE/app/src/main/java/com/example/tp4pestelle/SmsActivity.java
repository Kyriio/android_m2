package com.example.tp4pestelle;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SmsActivity extends AppCompatActivity {

    private static final String KEY_FROM = "keyFrom";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_main);


        Button buttonAppel = findViewById(R.id.ValidateButtonSMS);
        buttonAppel.setOnClickListener(v -> {
            EditText editTextNumero = findViewById(R.id.NumberSmS);
            EditText editSms = findViewById(R.id.SmsEdit);
            String NumString = editTextNumero.getText().toString();
            String sms = editSms.getText().toString();

            if (NumString.length() == 10 && NumString.matches("\\d+") && !sms.isEmpty()){

                Intent smsintent = new Intent(Intent.ACTION_SENDTO);
                smsintent.putExtra("sms_body", sms);
                smsintent.setData(Uri.parse("smsto:"+NumString));

                try {
                    startActivity(smsintent);
                    Intent retourIntent = new Intent();
                    retourIntent.putExtra(KEY_FROM, "GOOOD IL ENVOIE SMS");
                    setResult(RESULT_OK, retourIntent);

                } catch (ActivityNotFoundException e) {
                    Toast.makeText(this, "Pas d'application pour SMS", Toast.LENGTH_SHORT).show();
                    Intent retourIntent = new Intent();
                    retourIntent.putExtra(KEY_FROM, "SMS_BAD");
                    setResult(RESULT_CANCELED, retourIntent);
                } finally {
                    finish();
                }

            } else {
                Toast.makeText(this, "Votre numéro ne comporte pas 10 chiffres / Vous n'avez pas de message", Toast.LENGTH_SHORT).show();
            }

        });


        Button cancelButton = findViewById(R.id.ButtonCancelSMS);
        cancelButton.setOnClickListener(v -> {
            Intent retourIntent = new Intent();
            retourIntent.putExtra(KEY_FROM, "SMS_CANCEL");
            setResult(RESULT_CANCELED, retourIntent);
            finish();
        });

    }




}
