package com.example.tp4pestelle;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

public class PhoneActivity extends AppCompatActivity {

    private static final String KEY_FROM = "keyFrom";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_main);


        Button buttonAppel = findViewById(R.id.ValidateButton);
        buttonAppel.setOnClickListener(v -> {
            EditText editTextNumero = findViewById(R.id.NumberEdit);
            String NumString = editTextNumero.getText().toString();

            Log.d("STIRNG", "onCreate: " + NumString.length());
            if (NumString.length() == 10 && NumString.matches("\\d+")) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + NumString));
                try {
                    startActivity(intent);
                    Intent retourIntent = new Intent();
                    retourIntent.putExtra(KEY_FROM, "GOOOD IL APPEL");
                    setResult(RESULT_OK, retourIntent);

                } catch (ActivityNotFoundException e) {
                    Toast.makeText(this, "Pas d'application pour appeler", Toast.LENGTH_SHORT).show();
                    Intent retourIntent = new Intent();
                    retourIntent.putExtra(KEY_FROM, "APPEL_BAD");
                    setResult(RESULT_CANCELED, retourIntent);
                } finally {
                    finish();
                }

            } else {
                Toast.makeText(this, "Votre numéro ne comporte pas 10 chiffres", Toast.LENGTH_SHORT).show();
            }
        });




        Button cancelButton = findViewById(R.id.ButtonCancel);
        cancelButton.setOnClickListener(v -> {
            Intent retourIntent = new Intent();
            retourIntent.putExtra(KEY_FROM, "APPEL_CANCEL");
            setResult(RESULT_CANCELED, retourIntent);
            finish();
        });
    }
}
