package com.example.tp4pestelle;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button callButton = findViewById(R.id.CallButton);
        Button smsButton = findViewById(R.id.SMSButton);
        Button MMSButton = findViewById(R.id.MMSButton);
        Button WEBButton = findViewById(R.id.WEBButton);
        Button GeoButton = findViewById(R.id.GEOButton);

        ActivityResultLauncher<Intent> launcher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {
                            String keyFrom = data.getStringExtra("keyFrom");
                            Log.d("GOOD", "onCreate: " + keyFrom);
                            Toast.makeText(MainActivity.this, "Action réussie " + keyFrom, Toast.LENGTH_SHORT).show();

                        }
                    } else if (result.getResultCode() == RESULT_CANCELED) {
                        Intent data = result.getData();
                        if (data !=null){
                            String keyFrom = data.getStringExtra("keyFrom");
                            Log.d("BAD", "onCreate: "+keyFrom);
                            Toast.makeText(MainActivity.this, "Action cancel", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
        );

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Toast.makeText(MainActivity.this, "APPEL", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, PhoneActivity.class);
                    launcher.launch(intent);

                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        smsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Toast.makeText(MainActivity.this, "MMS/SMS", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this,SmsActivity.class);
                    launcher.launch(intent);
                } catch (Exception e){
                    Toast.makeText(MainActivity.this,"FAILED", Toast.LENGTH_SHORT).show();
                }
            }
        });

        MMSButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Toast.makeText(MainActivity.this, "MMS/SMS", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this,SmsActivity.class);
                    launcher.launch(intent);
                } catch (Exception e){
                    Toast.makeText(MainActivity.this,"FAILED", Toast.LENGTH_SHORT).show();
                }
            }
        });


        WEBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Toast.makeText(MainActivity.this,"WEB", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this,WebActivity.class);
                    launcher.launch(intent);
                }catch (Exception e){
                    Toast.makeText(MainActivity.this,"FAILED", Toast.LENGTH_SHORT).show();
                }
            }
        });

        GeoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Toast.makeText(MainActivity.this,"GEO", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this,GeoActivity.class);
                    launcher.launch(intent);

                }catch (Exception e){
                    Toast.makeText(MainActivity.this,"FAILED", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    /*
    private void PhoneNumber(String phoneNumber){

        try{
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + phoneNumber));
            startActivity(intent);

        }catch (Exception e){
            Toast.makeText(MainActivity.this,"Erreur, lancement impossible",Toast.LENGTH_SHORT).show();
        }

    }

    private void SendSMS(String sms){
        try {
            Intent smsintent = new Intent(Intent.ACTION_SENDTO);
            smsintent.setData(Uri.parse("smsto:"+sms));
            startActivity(smsintent);

        }catch (Exception e){
            Toast.makeText(MainActivity.this,"Erreur, lancement impossible",Toast.LENGTH_SHORT).show();
        }
    }


    private void SendMMS(String phoneNumber){
        try{
            Uri uri = Uri.parse("mmsto:"+phoneNumber);
            Intent mmsIntent = new Intent(Intent.ACTION_SENDTO, uri);
            mmsIntent.putExtra("sms_body", "MMS ici");
            startActivity(mmsIntent);

        }catch (Exception e){
            Toast.makeText(MainActivity.this,"Erreur, lancement impossible",Toast.LENGTH_SHORT).show();
        }

    }

    private void ToWeb(String myUrl){

        try {
            Uri webpage = Uri.parse(myUrl);
            Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
            startActivity(webIntent);

        }catch (Exception e){
            Toast.makeText(MainActivity.this,"Erreur, lancement impossible",Toast.LENGTH_SHORT).show();
        }

    }

    private void geoLocation(String myGeo){
        try {
            Uri geo = Uri.parse(myGeo);
            Intent geoIntent = new Intent(Intent.ACTION_VIEW, geo);
            startActivity(geoIntent);

        }catch (Exception e){
            Toast.makeText(MainActivity.this,"Erreur, lancement impossible",Toast.LENGTH_SHORT).show();
        }
    }
*/



}