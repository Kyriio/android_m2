package com.example.tp4pestelle;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class GeoActivity extends AppCompatActivity {

    private static final String KEY_FROM = "keyFrom";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.geo_main);

        Button buttonWeb = findViewById(R.id.ValidateButtonGEO);
        buttonWeb.setOnClickListener(v -> {
            EditText editLat = findViewById(R.id.LatitudeEdit);
            String LatString = editLat.getText().toString();
            EditText editLong = findViewById(R.id.LongitudeEdit);
            String LongString = editLong.getText().toString();

            if (!LatString.isEmpty() && !LongString.isEmpty()){
                if(LatString.matches("[0-9]*\\.[0-9]*") && LongString.matches("[0-9]*\\.[0-9]*")){
                    Uri geo = Uri.parse("geo:"+LatString+','+LongString);
                    Intent geoIntent = new Intent(Intent.ACTION_VIEW, geo);

                    Log.d("GEOVALUE", "onCreate: " + geo.toString());

                    try {
                        startActivity(geoIntent);
                        Intent retourIntent = new Intent();
                        retourIntent.putExtra(KEY_FROM, "GOOOD IL A GEO");
                        setResult(RESULT_OK, retourIntent);

                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(this, "Pas d'application pour chercher / vérifiez que vos coordonnées existent", Toast.LENGTH_SHORT).show();
                        Intent retourIntent = new Intent();
                        retourIntent.putExtra(KEY_FROM, "GEO_BAD");
                        setResult(RESULT_CANCELED, retourIntent);
                    } finally {
                        finish();
                    }

                }else {
                    Toast.makeText(this,"Coordonnées non valides", Toast.LENGTH_SHORT).show();
                }

            }else {
                Toast.makeText(this,"Veuillez renseigner tous les champs",Toast.LENGTH_SHORT).show();
            }


        });



        Button cancelButton = findViewById(R.id.ButtonCancelGEO);
        cancelButton.setOnClickListener(v -> {
            Intent retourIntent = new Intent();
            retourIntent.putExtra(KEY_FROM, "GEO_CANCEL");
            setResult(RESULT_CANCELED, retourIntent);
            finish();
        });
    }
}