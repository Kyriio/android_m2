package com.example.tp4pestelle;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class WebActivity extends AppCompatActivity {

    private static final String KEY_FROM = "keyFrom";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_main);


        Button buttonWeb = findViewById(R.id.ValidateButtonWEB);
        buttonWeb.setOnClickListener(v -> {
            EditText editURL = findViewById(R.id.URLEdit);
            String URLString = editURL.getText().toString();

            Log.d("WEB", "onCreate: " + editURL);

            if (URLString.length() != 0){
                if ( URLString.startsWith("http://") || URLString.startsWith("https://")){
                    Uri webpage = Uri.parse(URLString);
                    Intent webIntent = new Intent(Intent.ACTION_VIEW,webpage);
                    //webIntent.setData(Uri.parse("web:" + URLString));

                    try {
                        startActivity(webIntent);
                        Intent retourIntent = new Intent();
                        retourIntent.putExtra(KEY_FROM, "GOOOD IL A RECHERCHE");
                        setResult(RESULT_OK, retourIntent);

                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(this, "Pas d'application pour chercher / vérifiez que votre URL est valide", Toast.LENGTH_SHORT).show();
                        Intent retourIntent = new Intent();
                        retourIntent.putExtra(KEY_FROM, "RECHERCHE_BAD");
                        setResult(RESULT_CANCELED, retourIntent);
                    } finally {
                        finish();
                    }
                }else{
                    Toast.makeText(this,"Veuillez rentrer une URL http:// ou https:// ",Toast.LENGTH_SHORT).show();
                }


            }else {
                Toast.makeText(this,"URL VIDE", Toast.LENGTH_SHORT).show();
            }


        });

        Button cancelButton = findViewById(R.id.ButtonCancelWEB);
        cancelButton.setOnClickListener(v -> {
            Intent retourIntent = new Intent();
            retourIntent.putExtra(KEY_FROM, "WEB_CANCEL");
            setResult(RESULT_CANCELED, retourIntent);
            finish();
        });


    }
}