package com.example.calcpestelle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Erase(View view) {

        EditText nb1 = (EditText) findViewById(R.id.Number1);
        EditText nb2 = (EditText) findViewById(R.id.Number2);

        nb1.getText().clear();
        nb2.getText().clear();
    }

    public void Calc(View view) {

        TextView resText = (TextView) findViewById(R.id.res);

        try{
            //On selectionne le group + l'id de celui selectionné
            RadioGroup radioGroup = findViewById(R.id.radioGroup);
            int selectedId = radioGroup.getCheckedRadioButtonId();
            RadioButton radioButton = (RadioButton) findViewById(selectedId);

            //On prend la valeur du nb1 / nb2
            EditText nb1 = findViewById(R.id.Number1);
            Double nb1Value = Double.parseDouble(nb1.getText().toString());

            EditText nb2 = findViewById(R.id.Number2);
            Double nb2Value = Double.parseDouble(nb2.getText().toString());

            Log.d("NB1", "nb: " + nb1Value);
            Log.d("NB2", "nb: " + nb2Value);

            Log.d("BUTTONVALUE", ""+radioButton.getText().toString());


            switch (radioButton.getText().toString()) {
                case "plus":
                    double add = nb1Value + nb2Value;
                    String texteAddition = Double.toString(add);

                    String concat = getString(R.string.resultats) +" "+ texteAddition;
                    resText.setText(concat);
                    break;

                case "moins":
                    double soustraction = nb1Value - nb2Value;
                    String texteSoustraction = Double.toString(soustraction);
                    String concat2 = getString(R.string.resultats) +" "+ texteSoustraction;
                    resText.setText(concat2);
                    break;

                case "multiplié":
                    double multiplication = nb1Value * nb2Value;
                    String texteMultiplication = Double.toString(multiplication);
                    String concat3 = getString(R.string.resultats) +" "+ texteMultiplication;
                    resText.setText(concat3);
                    break;

                case "divisé":

                    String texteDivision = "";

                    if (nb2Value != 0) {
                        double division = nb1Value / nb2Value;
                        texteDivision = Double.toString(division);
                    } else {
                        texteDivision = "Division par zéro impossible";
                    }

                    String concat4 = getString(R.string.resultats) +" "+ texteDivision;
                    resText.setText(concat4);
                    break;

            }


        }catch (NumberFormatException e) {
            Log.d("FAIL", "FAIL: ");
            String def = "Veuillez rentrer deux nombres";
            resText.setText(def);
        }
    }

    public void QuitApplication(View view) {
        finish();
        System.exit(0);
    }
}