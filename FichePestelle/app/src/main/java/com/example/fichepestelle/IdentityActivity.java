package com.example.fichepestelle;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;


public class IdentityActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.identity_activity);

        //On récupère l'intent

        Intent intent = getIntent();

        //Petit try and catch pour voir si on a déjà des valeurs
        try {

            //On récupère tous nos champs
            EditText myName = (EditText) findViewById(R.id.TextFirstNameEdit);
            EditText mySecondName = (EditText) findViewById(R.id.SecondNameEdit);
            EditText myPhone = (EditText) findViewById(R.id.PhoneEdit);

            //On récupère par le biais de l'intent les valeurs
            String NameReceive = intent.getStringExtra("CurrentName");
            String SecondNameReceive = intent.getStringExtra("CurrentSecondName");
            String PhoneReceive = intent.getStringExtra("CurrentPhone");

            //On set up les textes
            myName.setText(NameReceive);
            mySecondName.setText(SecondNameReceive);
            myPhone.setText(PhoneReceive);

        }catch (Exception e){
            //petit log pour regarder si on a rien reçus
            Log.d("RECEIVE", "onCreate: NOTHING TO PUT ON FIELDS");
        }
    }

    //Ici, quand on clique sur le bouton modifier
    public void SendModif(View view){
        //Ici pareil que dans l'adresse, je pense que c'est bien de laisser le choix à l'utilisateur
        //De choisir ce qu'il veux mettre ou non dans les choix
        //On prend toutes les modifications
        String ModifiedName = ((EditText) findViewById(R.id.TextFirstNameEdit)).getText().toString();
        String ModifiedSecondName = ((EditText) findViewById(R.id.SecondNameEdit)).getText().toString();
        String ModifiedPhone = ((EditText) findViewById(R.id.PhoneEdit)).getText().toString();

        //On créé le nouvel intent
        Intent resultIntent = new Intent();

        //On passe toutes les modifications avec des clefs pour pouvoir tout modifier de l'autre côté
        resultIntent.putExtra("modifiedName", ModifiedName);
        resultIntent.putExtra("modifiedSecondName", ModifiedSecondName);
        resultIntent.putExtra("Phone", ModifiedPhone);

        //On set le résultat sur ok et on ferme l'activité
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    //Petite fonction pour cancel l'edit au cas où
    public void CancelEdit(View view){
        //set du résultat sur cancel et on fait finir l'activité
        setResult(RESULT_CANCELED);
        finish();
    }
}