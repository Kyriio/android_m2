package com.example.fichepestelle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //Déclaration de mes deux Activité Launcher pour me permettre de les gérer séparemment
    private ActivityResultLauncher<Intent> Identitylauncher;
    private ActivityResultLauncher<Intent> Addresslauncher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Déclaration du 1er pour l'idendité, qui me permet aussi de mettre à jour les différents
        // champs si l'utilisateur veut à nouveau les mettres à vide
        Identitylauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {

                            //On récupère les strings avec nos mots clefs passé par l'intent
                            String modifiedName = data.getStringExtra("modifiedName");
                            String modifiedSecondName = data.getStringExtra("modifiedSecondName");
                            String Phone = data.getStringExtra("Phone");

                            //Ici on va update l'identité pour que ce soit plus propre dans une fonction
                            upDateIdentity(modifiedName,modifiedSecondName,Phone);


                        }
                    }
                }
        );

        //Ici ça va être le launcher de l'address pour pouvoir modifier les champs
        Addresslauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {
                            //Pareil ici même si l'utilisateur met à blanc un champ on le récupère au cas où
                            // il veut le mettre à blanc
                            String modifiedNumber = data.getStringExtra("ModifiedNumber");
                            String modifiedStreet = data.getStringExtra("ModifiedStreet");
                            String modifiedPostal = data.getStringExtra("ModifiedPostal");
                            String modifiedCity = data.getStringExtra("ModifiedCity");

                            //Update de l'adresse pour plus de propreté
                            upDateAddress(modifiedNumber,modifiedStreet,modifiedPostal,modifiedCity);


                        }
                    }
                }
        );
    }

    //Fonction pour update l'adresse
    public void upDateAddress(String number,String street,String Postal,String City){

        //On récupère tous nos textView et on va setText avec nos paramètres qui sont les changements
        TextView myNumber = (TextView) findViewById(R.id.TextNumberadd);
        TextView myStreet = (TextView) findViewById(R.id.StreetField);
        TextView myPostal = (TextView) findViewById(R.id.PostalField);
        TextView myCity = (TextView) findViewById(R.id.CityField);

        myNumber.setText(number);
        myStreet.setText(street);
        myPostal.setText(Postal);
        myCity.setText(City);


    }

    //Fonction pour update l'identité
    public void upDateIdentity(String Name,String SecondName, String Phone){

        //Pareil que updateAddrress, ça va nous permettre de cette fois-ci mettre à jour l'identité
        TextView myName = (TextView) findViewById(R.id.TextFirstName);
        TextView mySecondName = (TextView) findViewById(R.id.SecondName);
        TextView myPhone = (TextView) findViewById(R.id.Phone);

        myName.setText(Name);
        mySecondName.setText(SecondName);
        myPhone.setText(Phone);
    }


    //Ici Fonction qui permet de changer d'activité
    public void ModifyIdentity(View view) {

        //On définit un nouvel indent
        Intent intent = new Intent(this, IdentityActivity.class);

        //Ici, petit Try and catch pour regarder si des valeurs sont déjà dans nos champs pour les
        //Envoyer dans l'intent
        try {

            //On prend les texts
            String CurrentName = ((TextView) findViewById(R.id.TextFirstName)).getText().toString();
            String CurrentSecondName = ((TextView) findViewById(R.id.SecondName)).getText().toString();
            String CurrentPhone = ((TextView) findViewById(R.id.Phone)).getText().toString();

            //Envoie dans l'intent
            intent.putExtra("CurrentName",CurrentName);
            intent.putExtra("CurrentSecondName",CurrentSecondName);
            intent.putExtra("CurrentPhone",CurrentPhone);

        }catch (Exception e){
            //Ici on peux regarder si on remplis rien
            Log.d("OOPSIE", "NO STRING ");
        }

        //On lance l'activité pour modifier l'identité
        Identitylauncher.launch(intent);

    }

    //Ici Fonction qui permet de changer d'activité ici, pour modifier l'adresse
    public void ModifyAddress(View view){

        //Nouvel Intent
        Intent intent = new Intent(this, AddressActivity.class);

        //Ici, encore un try and catch pour regarder si on a déjà des valeurs
        try {
            //On récupère les infos déjà rentrées si elles existent
            String CurrentNumber = ((TextView) findViewById(R.id.TextNumberadd)).getText().toString();
            String CurrentStreet = ((TextView) findViewById(R.id.StreetField)).getText().toString();
            String CurrentPostal = ((TextView) findViewById(R.id.PostalField)).getText().toString();
            String CurrentCity= ((TextView) findViewById(R.id.CityField)).getText().toString();

            //On envoie tous dans l'intent avec des mots clefs forcémment
            intent.putExtra("CurrentNumber",CurrentNumber);
            intent.putExtra("CurrentStreet",CurrentStreet);
            intent.putExtra("CurrentPostal",CurrentPostal);
            intent.putExtra("CurrentCity",CurrentCity);

        }catch (Exception e){
            //Ici s'il y a rien on peux regarder
            Log.d("OOPSIE", "NO STRING ");
        }

        //On lance l'activité en attendant un résultat
        Addresslauncher.launch(intent);

    }

    //Fonction en plus pour quitter l'application
    public void LeaveApp(View view){
        //On ferme toutes les activités
        //Et on quitte l'application proprement
        finishAffinity();
        System.exit(0);
    }

}