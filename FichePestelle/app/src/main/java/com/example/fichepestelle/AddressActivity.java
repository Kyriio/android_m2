package com.example.fichepestelle;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class AddressActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_activity);

        //Ici on va récupérer l'intent
        Intent intent = getIntent();

        //Petit try and catch pour set up si on a déjà des valeurs
        try {
            //Ici on récupère tous nos champs d'edit
            EditText myNumber = (EditText) findViewById(R.id.NumberEdit);
            EditText myStreet = (EditText) findViewById(R.id.StreetFieldEdit);
            EditText myPostal = (EditText) findViewById(R.id.PostalFieldEdit);
            EditText myCity = (EditText) findViewById(R.id.CityFieldEdit);

            //On récupère ici, tous les champs passés par l'intent number etc.
            String NumberReceive = intent.getStringExtra("CurrentNumber");
            String StreetReceive = intent.getStringExtra("CurrentStreet");
            String PostalReceive = intent.getStringExtra("CurrentPostal");
            String CityReceive = intent.getStringExtra("CurrentCity");

            //Ici, on va set up nos texte
            myNumber.setText(NumberReceive);
            myStreet.setText(StreetReceive);
            myPostal.setText(PostalReceive);
            myCity.setText(CityReceive);

        }catch (Exception e){
            //Ici, si on a rien on log pour être sûr
            Log.d("RECEIVE", "onCreate: NOTHING TO PUT ON FIELDS");
        }


    }


    //Ici, fonction quand on clique sur le bouton
    public void SendModifAddress(View view){
        //Ici j'ai pas fait de try and catch car je pense qu'on devrait laisser la possibilité
        //De ne pas remplir certains champs à l'utilisateur
        //On récupère tous nos champs
        String ModifiedNumber = ((EditText) findViewById(R.id.NumberEdit)).getText().toString();
        String ModifiedStreet= ((EditText) findViewById(R.id.StreetFieldEdit)).getText().toString();
        String ModifiedPostal = ((EditText) findViewById(R.id.PostalFieldEdit)).getText().toString();
        String ModifiedCity = ((EditText) findViewById(R.id.CityFieldEdit)).getText().toString();

        //On créé l'intent
        Intent resultIntent = new Intent();

        //On envoie toutes nos valeurs dans l'intent
        resultIntent.putExtra("ModifiedNumber", ModifiedNumber);
        resultIntent.putExtra("ModifiedStreet", ModifiedStreet);
        resultIntent.putExtra("ModifiedPostal", ModifiedPostal);
        resultIntent.putExtra("ModifiedCity", ModifiedCity);

        //On envoie un bon résultat et on ferme l'activité
        setResult(RESULT_OK, resultIntent);
        finish();

    }

    //Ici, fonction pour cancel l'edition de l'adresse
    public void CancelEditAddress(View view){

        //On set le result sur cancel et on ferme l'activité
        setResult(RESULT_CANCELED);
        finish();
    }
}
