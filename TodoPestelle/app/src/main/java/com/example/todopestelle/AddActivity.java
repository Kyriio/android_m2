package com.example.todopestelle;



import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class AddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_activity);


    }

    //Ici, on a juste à envoyer la données qui est entrées
    public void SendNewTask(View view){

        String Task =  ((EditText) findViewById(R.id.TaskText)).getText().toString();

        Intent resultIntent = new Intent();

        //On envoie la nouvelle tâche dans un putExtra
        resultIntent.putExtra("AddedOne", Task);


        //On set le résultat sur ok et on ferme l'activité
        setResult(RESULT_OK, resultIntent);
        finish();

    }

    //ici si on appuie sur le bouton cancel
    public void Cancel(View view){

        //set du résultat sur cancel et on fait finir l'activité
        setResult(RESULT_CANCELED);
        finish();

    }
}