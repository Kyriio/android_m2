package com.example.todopestelle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ActivityResultLauncher<Intent> addLauncher;
    private ArrayList<String> entrees;

    private ArrayAdapter<String> adapter;

    private Toolbar toolbar;

    private ListView myList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Initialisation de mes variables
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myList = findViewById(R.id.MyList);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //Récupération du bouton flottant pour lancer l'activité
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Bouton appuyé", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                addLauncher.launch(intent);
            }
        });

        //définition du launcher
        addLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent data = result.getData();

                        //Si on reçoit quelque chose de non null / vide on l'ajoute à notre liste !
                        if (data != null) {
                            String addedString = data.getStringExtra("AddedOne");
                            Log.d("DATA", "onCreate: " + addedString);
                            Toast.makeText(MainActivity.this, addedString, Toast.LENGTH_SHORT).show();

                            assert addedString != null;
                            if (addedString.length() != 0){
                                entrees.add(addedString);
                                adapter.notifyDataSetChanged();
                                Log.d("TOOLBAR", "Toolbar before configure: " + toolbar); // Ajout du log
                                setSupportActionBar(toolbar);
                                Log.d("TOOLBAR", "Toolbar after configure: " + toolbar); // Ajout du log
                            }

                        }
                    }
                });

        //Ici, on charge les données déjà faites par l'utilisateur
        entrees = new ArrayList<>();
        loadList();

        //Petit adapter pour cocher les cases
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, entrees);
        myList.setAdapter(adapter);
    }



    //initialisation du menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Ici, quand on clique sur supprimer / sauvegarder
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //Ici on fait un try / catch pour pouvoir delete selon la position et non pas le texte des données
        try{

            int id = item.getItemId();

            if (id == R.id.action_delete) {
                ArrayList<String> selection = new ArrayList<String>();

                SparseBooleanArray checkedPositions = myList.getCheckedItemPositions();

                for (int i = 0; i < checkedPositions.size(); i++) {
                    int position = checkedPositions.keyAt(i);
                    if (checkedPositions.valueAt(i)) {
                        String selectedItem = entrees.get(position);
                        selection.add(selectedItem);
                    }
                }

                for (String selectedItem : selection) {
                    entrees.remove(selectedItem);
                }

                adapter.notifyDataSetChanged();

                for (int i = 0; i < myList.getCount(); i++) {
                    myList.setItemChecked(i, false);
                }

                return true;
            //Ici c'est si on veux sauvegarder on récupère la liste
            }else if( id == R.id.action_save){
                savedList();
                return  true;
            }

            return super.onOptionsItemSelected(item);

        }catch (Exception e){
            Log.d("DATA", "onOptionsItemSelected: plus de données ");
        }

        return false;

    }


    //Ici, on va créer un nouveau fichier pour sauvegarder nos données dans l'application
    private void savedList() {
        try {
            File file = new File(getExternalFilesDir(null), "saved_list.txt");
            FileWriter writer = new FileWriter(file);

            for (String entry : entrees) {
                writer.write(entry + "\n");
            }

            writer.flush();
            writer.close();

            Toast.makeText(this, "Liste sauvegardée avec succès", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Erreur lors de la sauvegarde de la liste", Toast.LENGTH_SHORT).show();
        }
    }

    //Ici on va chercher le fichier pour pouvoir ajouter à notre liste les données déjà rentrées auparavant
    private void loadList() {
        try {
            File file = new File(getExternalFilesDir(null), "saved_list.txt");
            if (file.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                String line;

                while ((line = reader.readLine()) != null) {
                    entrees.add(line);
                }

                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //au cas où pour la toolbar elle faisait n'importe quoi
    @Override
    protected void onResume() {
        super.onResume();

        setSupportActionBar(toolbar);
    }





}