package com.example.tpimagepestelle;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private ActivityResultLauncher<String> getContentLauncher;
    private ImageView imageView;
    private Button loadButton;

    private Toolbar toolbar;

    private Uri saved;
    private Bitmap originalBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Initialisation des variables

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView displayUri = findViewById(R.id.textView);

        imageView = findViewById(R.id.imageView);
        loadButton = findViewById(R.id.loadButton);

        registerForContextMenu(imageView);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Bouton correctement appuyé
        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("BOUTTON", "Bouton correctement appuyé");
            }
        });

        //Faire en sorte que le menu s'ouvre Bien
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContextMenu(imageView);
            }
        });

        //Launcher d'activité pour savoir si on a bien une image choisie
        getContentLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(),
                new ActivityResultCallback<Uri>() {
                    @Override
                    public void onActivityResult(Uri result) {
                        if (result != null) {
                            ChargerImage(result);
                            displayUri.setText(result.toString());
                            Log.d("IMGBIEN", "Image choisie : " + result.toString());
                        } else {
                            Log.d("IMGBAD", "Aucune image choisie");
                        }
                    }
                });

        // Load une image
        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContentLauncher.launch("image/*");
            }
        });

        Button reset = findViewById(R.id.resetButton);

        //Reset grâce à l'URI de l'image chargée
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChargerImage(saved);
            }
        });
    }

    //Ici, fonction pour charger l'image ( on sauvegarde son URI en passant pour le reset )
    private void ChargerImage(Uri imageUri) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable = true;

            saved = imageUri;
            Log.d("SAVED", "ChargerImage: ");
            originalBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri), null, options);

            if (originalBitmap != null) {
                imageView.setImageBitmap(originalBitmap);
            } else {
                Log.e("BITMAPBAD", "Erreur lors du chargement de l'image");
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("BITMAPURIBAD", "Erreur URI image");
        }
    }

    //Fonction pour les menus de la toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    //Fonction pour les menus context
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_context, menu);
    }

    //Ici pour faire le niveau de gris / inverser couleur du Context
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Bitmap currentBitmap = getBitmapFromImageView();

        if (currentBitmap == null) {
            Toast.makeText(this, "Aucune image chargée", Toast.LENGTH_SHORT).show();
            return super.onContextItemSelected(item);
        }

        int id = item.getItemId();

        if (id == R.id.inverser_couleurs) {
            Log.d("TEST", "onOptionsItemSelected: INVERSER COULEURS");
            Bitmap reversedImg = InverserCouleurs(currentBitmap);
            imageView.setImageBitmap(reversedImg);
            return true;
        } else if (id == R.id.niveaux_gris) {
            Log.d("TEST", "onOptionsItemSelected: Niveau gris");
            Bitmap ImgGris = ConvertirEnNiveauxGris(currentBitmap);
            imageView.setImageBitmap(ImgGris);
            return true;
        }

        return super.onContextItemSelected(item);
    }


    //Ici, on regarde ce qui a été selectionné dans la toolbar et on fait en fonction de l'ID
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bitmap currentBitmap = getBitmapFromImageView();

        if (currentBitmap == null) {
            Toast.makeText(this, "Aucune image chargée", Toast.LENGTH_SHORT).show();
            return super.onOptionsItemSelected(item);
        }

        int id = item.getItemId();

        Log.d("IDSELECTED", "onOptionsItemSelected: " + id);

        if (id == R.id.horizontal) {
            Log.d("TEST", "onOptionsItemSelected: HORIZONTAL ");
            Bitmap horizontalImage = HorizontalImage(currentBitmap);
            imageView.setImageBitmap(horizontalImage);
            return true;
        } else if (id == R.id.vertical) {
            Log.d("TEST", "onOptionsItemSelected: VERTICAL ");
            Bitmap verticalImage = VerticalImage(currentBitmap);
            imageView.setImageBitmap(verticalImage);
            return true;
        } else if (id == R.id.rotation_horaire) {
            Log.d("TEST", "onOptionsItemSelected: HORAIRE");
            Bitmap rotatedImage = Rota_horaire();
            imageView.setImageBitmap(rotatedImage);
            return true;
        } else if (id == R.id.rotatation_anti_horaire) {
            Log.d("TEST", "onOptionsItemSelected: ANTI-HORAIRE");
            Bitmap rotatedAntiImage = Rota_anti_horaire();
            imageView.setImageBitmap(rotatedAntiImage);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Ici, fonction pour convertir en Gris
    private Bitmap ConvertirEnNiveauxGris(Bitmap bitmap) {

        //récupération de la longueur + larger + pixel en fonction du bitmap
        int largeur = bitmap.getWidth();
        int longueur = bitmap.getHeight();
        int[] pixels = new int[largeur * longueur];

        bitmap.getPixels(pixels, 0, largeur, 0, 0, largeur, longueur);

        //ici, boucle pour changer tous les pixels et faire la moyenne = au gris
        for (int i = 0; i < pixels.length; i++) {
            int pixel = pixels[i];

            int alpha = Color.alpha(pixel);
            int red = Color.red(pixel);
            int green = Color.green(pixel);
            int blue = Color.blue(pixel);

            int moyenne = (red + green + blue) / 3;

            red = green = blue = moyenne;

            pixels[i] = Color.argb(alpha, red, green, blue);
        }

        //return un nouveau bitmap pour pas changer le 1er
        Bitmap grisBitmap = Bitmap.createBitmap(pixels, largeur, longueur, Bitmap.Config.ARGB_8888);
        return grisBitmap;
    }

    //Ici, fonction pour inverser les couleurs
    private Bitmap InverserCouleurs(Bitmap bitmap) {
        //récupération de la longueur + larger + pixel en fonction du bitmap
        int largeur = bitmap.getWidth();
        int longueur = bitmap.getHeight();
        int[] pixels = new int[largeur * longueur];

        bitmap.getPixels(pixels, 0, largeur, 0, 0, largeur, longueur);

        //Boucle pour prendre chaque channel RGB et mettre tout ça en négatif ( 255 - valeur de base pour avoir l'inverse)
        for (int i = 0; i < pixels.length; i++) {
            int pixel = pixels[i];

            int alpha = Color.alpha(pixel);
            int red = Color.red(pixel);
            int green = Color.green(pixel);
            int blue = Color.blue(pixel);

            red = 255 - red;
            green = 255 - green;
            blue = 255 - blue;

            pixels[i] = Color.argb(alpha, red, green, blue);
        }

        //bitmap qu'on renvoie
        Bitmap reverseBitmap = Bitmap.createBitmap(pixels, largeur, longueur, Bitmap.Config.ARGB_8888);
        return reverseBitmap;
    }

    //Ici, fonction pour mettre l'image de manière horizontale
    private Bitmap HorizontalImage(Bitmap bitmap) {
        //récupération de la longueur + larger + pixel en fonction du bitmap
        int largeur = bitmap.getWidth();
        int longueur = bitmap.getHeight();
        int[] pixels = new int[largeur * longueur];

        bitmap.getPixels(pixels, 0, largeur, 0, 0, largeur, longueur);

        //Double for pour inverser en fait la position des pixels d'où la variable "temp"
        // On fait en sorte de prendre le pixel à l'opposé et on le set
        for (int ligne = 0; ligne < longueur; ligne++) {
            for (int col = 0; col < largeur / 2; col++) {
                int temp = pixels[ligne * largeur + col];
                pixels[ligne * largeur + col] = pixels[ligne * largeur + (largeur - 1 - col)];
                pixels[ligne * largeur + (largeur - 1 - col)] = temp;
            }
        }

        //bitmap qu'on renvoie
        Bitmap horizontalBitmap = Bitmap.createBitmap(pixels, largeur, longueur, Bitmap.Config.ARGB_8888);
        return horizontalBitmap;
    }

    //Fonction pour faire l'image mais en vertical
    private Bitmap VerticalImage(Bitmap bitmap) {
        //récupération de la longueur + larger + pixel en fonction du bitmap
        int largeur = bitmap.getWidth();
        int longueur = bitmap.getHeight();
        int[] pixels = new int[largeur * longueur];

        bitmap.getPixels(pixels, 0, largeur, 0, 0, largeur, longueur);

        //ici c'est comme le vertical mais au lieu de prendre l'opposé en ligne ça sera celle de la colonne
        for (int ligne = 0; ligne < longueur / 2; ligne++) {
            for (int col = 0; col < largeur; col++) {
                int temp = pixels[ligne * largeur + col];
                pixels[ligne * largeur + col] = pixels[(longueur - 1 - ligne) * largeur + col];
                pixels[(longueur - 1 - ligne) * largeur + col] = temp;
            }
        }


        //bitmap qu'on renvoie
        Bitmap verticalBitmap = Bitmap.createBitmap(pixels, largeur, longueur, Bitmap.Config.ARGB_8888);
        return verticalBitmap;
    }

    private Bitmap Rota_horaire() {
        //récupération de la longueur + larger + pixel + les pixels sur lesquels on va appliquer une rotation en fonction du bitmap
        int largeur = originalBitmap.getWidth();
        int longueur = originalBitmap.getHeight();
        int[] pixels = new int[largeur * longueur];
        int[] rotatedPixels = new int[largeur * longueur];

        originalBitmap.getPixels(pixels, 0, largeur, 0, 0, largeur, longueur);

        //double for, qui va nous permettre de prendre les pixels et de les transformers pour que l'image fasse sa rotation en mode "horaire"
        for (int ligne = 0; ligne < longueur; ligne++) {
            for (int col = 0; col < largeur; col++) {
                rotatedPixels[col * longueur + (longueur - 1 - ligne)] = pixels[ligne * largeur + col];
            }
        }

        //bitmap qu'on renvoie
        Bitmap rotatedBitmap = Bitmap.createBitmap(rotatedPixels, longueur, largeur, Bitmap.Config.ARGB_8888);
        return rotatedBitmap;
    }


    //Fonction pour l'anti_horaire cette fois-ci
    private Bitmap Rota_anti_horaire() {
        //récupération de la longueur + larger + pixel + les pixels sur lesquels on va appliquer une rotation anti horaire en fonction du bitmap
        int largeur = originalBitmap.getWidth();
        int longueur = originalBitmap.getHeight();
        int[] pixels = new int[largeur * longueur];
        int[] rotatedPixels = new int[largeur * longueur];

        originalBitmap.getPixels(pixels, 0, largeur, 0, 0, largeur, longueur);

        //double for comme dans celle horaire mais cette fois-ci, on fait non pas une modification sur la ligne mais la colonne
        for (int ligne = 0; ligne < longueur; ligne++) {
            for (int col = 0; col < largeur; col++) {
                rotatedPixels[(largeur - 1 - col) * longueur + ligne] = pixels[ligne * largeur + col];
            }
        }
        //bitmap qu'on renvoie
        Bitmap rotatedBitmap = Bitmap.createBitmap(rotatedPixels, longueur, largeur, Bitmap.Config.ARGB_8888);
        return rotatedBitmap;
    }


    //Petite fonction pour avoir le bitmap actuel au lieu de devoir rechercher à chaque fois l'imageView
    private Bitmap getBitmapFromImageView() {
        if (imageView.getDrawable() instanceof BitmapDrawable) {
            Log.d("IMG", "getBitmapFromImageView: ");
            return ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        }
        return null;
    }
}
