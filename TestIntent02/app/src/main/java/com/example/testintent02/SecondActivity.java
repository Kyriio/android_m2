package com.example.testintent02;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        Intent intent = getIntent();
        String value = intent.getStringExtra("initialText");

        Log.d("MYVALUE", "onCreate: " + value);

        EditText myEdit = (EditText) findViewById(R.id.TextToModif);

        myEdit.setText(value);

    }

    public void SendModif(View view){

        String modifiedText = ((EditText) findViewById(R.id.TextToModif)).getText().toString();

        Intent resultIntent = new Intent();
        resultIntent.putExtra("modifiedText", modifiedText);

        setResult(RESULT_OK, resultIntent);

        finish();


    }



}