package com.example.testintent02;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ActivityResultLauncher<Intent> activityLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView myTextView = (TextView) findViewById(R.id.TextModif);

        activityLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {
                            String modifiedText = data.getStringExtra("modifiedText");
                            myTextView.setText(modifiedText);

                        }
                    }
                }
        );
    }

    public void ModifyMessage(View view) {

        Intent intent = new Intent(this, SecondActivity.class);
        String currentText = ((TextView) findViewById(R.id.TextModif)).getText().toString();
        intent.putExtra("initialText", currentText);
        activityLauncher.launch(intent);

    }
}