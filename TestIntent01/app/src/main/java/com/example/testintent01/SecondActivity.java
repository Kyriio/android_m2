package com.example.testintent01;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);


        Intent intent = getIntent();
        String value = intent.getStringExtra("key");

        if(!value.isEmpty()){

            TextView myText = (TextView) findViewById(R.id.textView);

            Log.d("VALUE", "onCreate: " + value);

            myText.setText(value);

        }


    }

}