package com.example.testintent01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    }


    public void SendMessage(View view) {
        EditText MyText = (EditText) findViewById(R.id.editTextText);
        String text = MyText.getText().toString();

        Log.v("MonMessage", "sendMessage: " + text);
        // Il s'affiche bien



        Intent myIntent = new Intent(this, SecondActivity.class);
        myIntent.putExtra("key", text);
        MainActivity.this.startActivity(myIntent);

        //Log.d("INDENT", "SendMessage: " + myIntent.toString());
        //


    }
}