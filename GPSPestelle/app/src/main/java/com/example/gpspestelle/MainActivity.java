package com.example.gpspestelle;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /* Déclaration de mes variables */
    private ActivityResultLauncher<String> getContentLauncher;
    private ImageView imageView;
    private Button LoadButton;

    private Uri saved;

    private Bitmap originalBitmap;

    private TextView longField;
    private TextView latField;

    private Button googleButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Initialisation de mes variables */
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);

        LoadButton = findViewById(R.id.button);

        longField = findViewById(R.id.longField);

        latField = findViewById(R.id.latField);

        googleButton = findViewById(R.id.loadGoogleMaps);

        /* Ici on va essayer de récupérer une appli pour avoir nos images */
        LoadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContentLauncher.launch("image/*");
            }
        });

        /* Ici le launcher pour après afficher les images + avoir les méta Datas */
        getContentLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(),
                new ActivityResultCallback<Uri>() {
                    @Override
                    public void onActivityResult(Uri result) {
                        if (result != null) {
                            ChargerImage(result);
                            getMetaData(result);

                            Log.d("IMGBIEN", "Image choisie : " + result.toString());
                        } else {
                            Log.d("IMGBAD", "Aucune image choisie");
                        }
                    }
                });

        /*Ici bouton si on appuie pour voir la localisation sur google */
        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String geoUri = "geo:" +latField.getText().toString()+","+longField.getText().toString();
                Uri location = Uri.parse(geoUri);

                Intent openGoogle =new Intent(Intent.ACTION_VIEW,location);
                openGoogle.setPackage("com.google.android.apps.maps");
                if (openGoogle.resolveActivity(getPackageManager()) != null) {
                    startActivity(openGoogle);
                }
            }
        });
    }

    /* Méthode pour charge mon Image avec un Bitmap*/
    private void ChargerImage(Uri imageUri) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable = true;

            saved = imageUri;
            Log.d("SAVED", "ChargerImage: ");
            originalBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri), null, options);

            if (originalBitmap != null) {
                imageView.setImageBitmap(originalBitmap);
            } else {
                Log.e("BITMAPBAD", "Erreur lors du chargement de l'image");
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("BITMAPURIBAD", "Erreur URI image");
        }
    }

    /* Méthode pour avoir les méta Datas de l'image chargée */
    private void getMetaData(Uri imageUri) {
        try {
            InputStream inputStream = getContentResolver().openInputStream(imageUri);
            if (inputStream != null) {
                ExifInterface exifInterface = new ExifInterface(inputStream);

                float[] latLong = new float[2];
                Log.d("TTTTTT", "getMetaData: " + exifInterface.getLatLong(latLong));

                /* Obtenir les coordonnées GPS */

                if (exifInterface.getLatLong(latLong)) {
                    float latitude = latLong[0];
                    float longitude = latLong[1];

                    String latitudeString = String.valueOf(latitude);
                    String longitudeString = String.valueOf(longitude);

                    Log.d("GPS", "Latitude : " + latitude + ", Longitude : " + longitude);
                    longField.setText(longitudeString);
                    latField.setText(latitudeString);
                } else {

                    Log.d("GPS", "Aucune information de coordonnées GPS disponible");
                }

                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}