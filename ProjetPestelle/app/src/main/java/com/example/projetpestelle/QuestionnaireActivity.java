package com.example.projetpestelle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class QuestionnaireActivity extends AppCompatActivity {

    /* Déclaration des variables */
    TextView titleTextView;
    TextView questionTextView;
    RadioGroup choicesRadioGroup;
    Button nextButton;
    Button abandonButton;
    List<Question> questions = new ArrayList<>();
    String quizzTitle;
    TextView nbDisplay;
    private int score = 0;
    private Integer numberQuestion;
    private Integer count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.questionnaire_play);

        /* Initilisation des variables */
        titleTextView = findViewById(R.id.titleTextView);
        questionTextView = findViewById(R.id.questionTextView);
        nextButton= findViewById(R.id.nextButton);
        choicesRadioGroup = findViewById(R.id.choicesRadioGroup);
        abandonButton = findViewById(R.id.abandonButton);
        nbDisplay = findViewById(R.id.nbDisplay);

        /* Ici, avant tout on récupère très important le nom du quizz précédemment cliqué */
        Intent myIntent = getIntent();
        quizzTitle = myIntent.getStringExtra("QUIZ_NAME");

        /* On set le titre du quizz */
        titleTextView.setText(quizzTitle);

        /* on Charge les questions */
        loadQuestions(quizzTitle);

        /* et on va appeler en boucle les questions */
        showQuestion();

        /* ici bouton quand on abandonne*/
        abandonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(QuestionnaireActivity.this,"Dommage ! Score : 0", Toast.LENGTH_SHORT).show();
                /* comme on a fait avant, on set le score à sauvegardé à 0 et le quizz comme complété !*/
                SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                String completed = quizzTitle +"_COMPLETED_QUESTIONNAIRES";
                String scoreKey = quizzTitle + "_SCORE";
                editor.putInt(completed,1);
                editor.putInt(scoreKey, 0);
                editor.apply();

                Intent intent = new Intent(QuestionnaireActivity.this, MainActivity.class);
                startActivity(intent);
                finishAffinity();
            }
        });

    }

    /* Fonction pour montrer les questions */
    private void showQuestion() {

        /* petit tricks ici, on va afficher à chaque questions qui a été précédemment "triée" */
        if (!questions.isEmpty()) {
            count +=1;

            /* donc là on mélange les question*/
            Collections.shuffle(questions);
            Question currentQuestion = questions.get(0);

            /* on affiche la question */
            questionTextView.setText(currentQuestion.getLibelleQuestion());

            /* ici on va juste set sur quelle question on se trouve*/
            String numberDisplay = ("Question : " + count + "/" + numberQuestion);
            nbDisplay.setText(numberDisplay);

            /* Ici par précaution, j'efface ce qu'il y a dans le radiogroupe */
            choicesRadioGroup.removeAllViews();

            /*Je crée et j'ajoute les RadioButton pour chaque choix dans le RadioGroup*/
            for (String choice : currentQuestion.getChoix()) {
                RadioButton radioButton = new RadioButton(this);
                radioButton.setText(choice);
                choicesRadioGroup.addView(radioButton);

            }

            /* Si on clique sur le nextButton */
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /* là je vérifie si il y a un radioButton selectionné */

                    int checkedRadioButtonId = choicesRadioGroup.getCheckedRadioButtonId();
                    RadioButton selectedRadioButton = findViewById(checkedRadioButtonId);
                    int selectedPosition = choicesRadioGroup.indexOfChild(selectedRadioButton);

                    if (selectedPosition != -1) {

                        Log.d("LOGIC", "onClick: Selected Position: " + selectedPosition);
                        Log.d("LOGIC", "onClick: Correct Index: " + currentQuestion.getReponseCorrecteIndex());

                        /* Si l'index correspond à l'index de la bonne réponse alors le score augmente */
                        if (selectedPosition == (currentQuestion.getReponseCorrecteIndex()-1)) {
                            // Réponse correcte, incrémente le score
                            Log.d("LOGIC", "onClick: OUI");
                            score++;
                        }

                        /* là j'appel la prochaine question de manière récursive */
                        showQuestion();
                    } else {
                        Toast.makeText(QuestionnaireActivity.this, "Veuillez sélectionner une réponse.", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            /* Retire la question affichée de la liste pour éviter de la répéter */
            questions.remove(0);
        } else {
            /* Si on a plus de question */
            Toast.makeText(this, "Score final : " + score, Toast.LENGTH_SHORT).show();

            /* On va stocker tout ça dans des clefs pour sauvegarder*/
            SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            String scoreKey = quizzTitle + "_SCORE";
            String completed = quizzTitle +"_COMPLETED_QUESTIONNAIRES";

            Log.d("COMPLETE", "showQuestion: " + completed);

            /* on set à done (1) et le score*/
            editor.putInt(completed,1);
            editor.putInt(scoreKey, score);
            editor.apply();

            /* On renvoie au 1er menu */
            Intent intent = new Intent(QuestionnaireActivity.this, MainActivity.class);
            startActivity(intent);
            finishAffinity();
        }
    }


    /* Fonction pour me permettre d'avoir mes questions de mes assets*/
    private void loadQuestions(String fileName) {
        /* on va chercher les assets avec forcémment les clefs pour regarder si elles sont déjà faites pour
        ne pas les re afficher !
         */
        File file = new File(getFilesDir(), fileName + ".txt");

        BufferedReader bufferedReader;
        SharedPreferences.Editor editor = null;
        SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        editor = preferences.edit();
        InputStream inputStream;

        /* Ici, on va regarder si le fichier est déjà ou non dans les assets
        * ce qui change forcémment la création ou non des questions !
        * */
        if (file.exists()){
            try{
                inputStream = openFileInput(fileName + ".txt");
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }else{
            try{
                AssetManager assetManager = getAssets();
                inputStream = assetManager.open(fileName+".txt");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }


        }

        try {

            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            Integer count = 0;
            Integer CorrectIndex = 0;
            String QuestionTitle =" ";
            ArrayList<String> choices = new ArrayList<>();

            /* Petit while pour bien trier mes assets et bien construire mes questions*/
            while ((line = bufferedReader.readLine()) != null) {

                count+=1;
                if (line.startsWith("Q;")){
                    String[] TestTitle  = line.split("\\;");
                    QuestionTitle = TestTitle[1].trim();
                    //Log.d("TITLE", "loadQuestions: " + QuestionTitle);
                }else if(line.startsWith("A;")){
                    String[] testChoice = line.split("\\;");
                    String AnswerTitle = testChoice[1].trim();

                    if(AnswerTitle.split(" ").length >1){
                        if(AnswerTitle.endsWith("x")){
                            CorrectIndex = count;
                            AnswerTitle = AnswerTitle.substring(0,AnswerTitle.length()-1);
                        }
                    }

                    choices.add(AnswerTitle);
                    //Log.d("TITLE", "loadQuestions: " + AnswerTitle);
                }
                if (choices.size() == 3){
                    Log.d("QUESTIONS", "loadQuestions: " + fileName + QuestionTitle + choices + (CorrectIndex-1));
                    questions.add(new Question(fileName,QuestionTitle,choices,(CorrectIndex-1)));
                    choices.clear();
                    count = 0;
                }
            }
            numberQuestion = questions.size();
            Log.d("TEST_SIZE", "loadQuestions: " + questions.size());
            editor.putInt(fileName + "_TOTAL_QUESTIONS", questions.size());
            editor.apply();

            inputStream.close();
        } catch (IOException e) {
            Log.e("QuestionnaireActivity", "Error reading questions", e);
        }
    }

}

