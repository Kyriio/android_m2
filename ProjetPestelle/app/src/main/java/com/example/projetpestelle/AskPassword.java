package com.example.projetpestelle;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AskPassword extends AppCompatActivity {

    /* délcaration des variables*/
    EditText password;
    Button sendpassword;
    Button cancel_MDP;

    private final String MY_PASS = "MDP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ask_pass);

        /* initialisation des variables*/
        password = findViewById(R.id.PASSWORD_FIELD);
        sendpassword = findViewById(R.id.sendPassword);
        cancel_MDP = findViewById(R.id.cancel_MDP);

        /* si le mot de passe est correcte, on permet à la personne d'aller sur la création de questionnaires*/
        sendpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!password.getText().toString().equals(MY_PASS)){
                    Toast.makeText(AskPassword.this,"Mauvais mot de passe !", Toast.LENGTH_SHORT).show();
                }else {
                    Intent myIntent = new Intent(AskPassword.this,CreateQuizzActivity.class);
                    startActivity(myIntent);
                }

            }
        });

        /* Ici, si la personne veut revenir à l'activité d'avant, elle peut */
        cancel_MDP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
