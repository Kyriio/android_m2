package com.example.projetpestelle;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ScoreActivity extends AppCompatActivity {

    /* déclaration des variables*/
    Button buttonOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score_display);

        /* Initialisation des variables */
        buttonOK = findViewById(R.id.buttonOK);
        int totalScore = 0;
        int totalQuestions = 0;
        ListView listView = findViewById(R.id.scoreListView);
        List<String> scoreList = new ArrayList<>();

        /* bouton pour quitter l'activité */
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);

        try {
            String[] questionnaireNames = getAssets().list("");


            for (String questionnaireName : questionnaireNames) {

                /* Ici on va lire les fichier et afficher le score et en plus on va prendre le nombre total de questions*/
                if (questionnaireName.endsWith(".txt")) {
                    questionnaireName = questionnaireName.substring(0, questionnaireName.length() - 4);
                    String scoreKey = questionnaireName + "_SCORE";
                    String totalQuestionsKey = questionnaireName + "_TOTAL_QUESTIONS";

                    int score = preferences.getInt(scoreKey, 0);
                    int questions = preferences.getInt(totalQuestionsKey, 0);

                    totalScore += score;
                    totalQuestions += questions;

                    Log.d("SCORE", "onCreate: score " + totalScore + " " + totalQuestions);

                    String scoreText = "Questionnaire " + questionnaireName + "\n Score " + score + " / " + questions;
                    scoreList.add(scoreText);
                }
            }

            /* Ici, on prend en compte aussi les fichiers interne */
            String[] internalFiles = fileList();
            for (String fileInterne : internalFiles) {
                if (fileInterne.endsWith(".txt")) {
                    fileInterne = fileInterne.replace(".txt","");
                    String scoreKey = fileInterne + "_SCORE";
                    String totalQuestionsKey = fileInterne + "_TOTAL_QUESTIONS";

                    int score = preferences.getInt(scoreKey, 0);
                    int questions = preferences.getInt(totalQuestionsKey, 0);

                    totalScore += score;
                    totalQuestions += questions;

                    Log.d("SCORE", "onCreate: score " + totalScore + " " + totalQuestions);

                    String scoreText = "Questionnaire " + fileInterne + "\n Score " + score + " / " + questions;
                    scoreList.add(scoreText);
                }
            }

            /* On va faire ici la somme des questions */
            if (totalQuestions > 0) {
                double averageScore = ((double) totalScore / totalQuestions) * 20;
                scoreList.add("\n");
                String averageText = "Moyenne générale : " + String.format("%.2f", averageScore) + " / 20";
                scoreList.add(averageText);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        /* Ici petit adaptater pour mettre tout ça dans ma listView */
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, scoreList);
        listView.setAdapter(arrayAdapter);
    }
}
