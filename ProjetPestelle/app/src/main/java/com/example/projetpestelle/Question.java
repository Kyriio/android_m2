package com.example.projetpestelle;

import java.io.Serializable;
import java.util.ArrayList;

/* Ma classe Question*/
public class Question implements Serializable {
    /* déclaration des variables */
    private String categorie;
    private String libelleQuestion;
    private String[] choix;
    private int reponseCorrecteIndex;


    /* Constructeur */
    public Question(String categorie, String libelleQuestion, ArrayList<String> choix, int reponseCorrecteIndex) {
        this.categorie = categorie;
        this.libelleQuestion = libelleQuestion;
        this.choix = choix.toArray(new String[3]);
        this.reponseCorrecteIndex = reponseCorrecteIndex;
    }

    /* Getters */
    public String getCategorie() {
        return categorie;
    }

    /* Getters */
    public String getLibelleQuestion() {
        return libelleQuestion;
    }

    /* Getters */
    public String[] getChoix() {
        return choix;
    }

    /* Getters */
    public int getReponseCorrecteIndex() {
        return reponseCorrecteIndex;
    }
}
