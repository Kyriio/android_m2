package com.example.projetpestelle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;


public class MainActivity extends AppCompatActivity {

    /*Déclaration des variables*/
    private Button testQCM;
    private Button showScore;
    private Button ReniScore;
    private Button GotoAdmin;
    private Button LeaveButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Initilisation des variables*/
        testQCM = findViewById(R.id.QCM_to_activity);
        showScore = findViewById(R.id.Score_to_activity);
        ReniScore = findViewById(R.id.reni_score_to_activity);
        GotoAdmin = findViewById(R.id.admin_to_activity);
        LeaveButton = findViewById(R.id.LeaveButton);

        /* Quand on clique sur le bouton on va sur l'activité correspondante */
        testQCM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this,DisplayQuizzActivity.class);
                startActivity(myIntent);
            }
        });

        showScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this,ScoreActivity.class);
                startActivity(myIntent);
            }
        });

        ReniScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* J'ai utilisé SharedPreferences car c'est beaucoup plus simple pour stocker des variables tel que le score etc */
                SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                /* ce que je fais ici, c'est que je prends toutes les clefs ( score / done ) et je reset pour que le score ainsi que l'accès aux questionnaire
                    soient rétablis !
                 */
                try {
                    String[] questionnaireNames = getAssets().list("");
                    for (String questionnaireName : questionnaireNames) {
                        if (questionnaireName.endsWith(".txt")) {
                            questionnaireName = questionnaireName.substring(0, questionnaireName.length() - 4);
                            String scoreKey = questionnaireName + "_SCORE";
                            String doneKey = questionnaireName+ "_COMPLETED_QUESTIONNAIRES";
                            editor.putInt(scoreKey, 0);
                            editor.putInt(doneKey,0);
                        }
                    }

                    /* Ici, on regarde aussi les fichiers interne pour reset */
                    String[] internalFiles = fileList();
                    for (String fileInterne : internalFiles) {
                        if (fileInterne.endsWith(".txt")) {
                            fileInterne = fileInterne.replace(".txt","");
                            String scoreKey = fileInterne+"_SCORE";
                            String doneKey = fileInterne+ "_COMPLETED_QUESTIONNAIRES";
                            editor.putInt(scoreKey, 0);
                            editor.putInt(doneKey,0);
                        }
                    }

                    editor.apply();

                    Toast.makeText(MainActivity.this,"Votre score a été réinitialisé avec succès !", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this,"Vous n'avez pas encore de score !", Toast.LENGTH_SHORT).show();
                }


            }
        });

        GotoAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this,AskPassword.class);
                startActivity(myIntent);
            }
        });

        /* Ici, on quitte l'application*/
        LeaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        });
    }
}