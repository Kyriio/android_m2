package com.example.projetpestelle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DisplayQuizzActivity extends AppCompatActivity {

    /* déclaration des variables */
    ListView DisplayQuizz;
    Button backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_quizz);

        /* Initialisation des variables */
        DisplayQuizz = findViewById(R.id.displayQuizz);
        backButton = findViewById(R.id.backButton);

        displayQuizzList();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /* Fonction qui permet d'aller chercher dans mes assets, les fichiers QCM */
    private void displayQuizzList() {
        AssetManager assetManager = getAssets();

        try {
            List<String> quizNames = new ArrayList<>();
            SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);

            /* Ici du coup, grâce aux clefs qu'on a faites avant,
                on peut regarder les questionnaires déjà fait et on ne les affichent pas !
             */
            for (String fileName : Objects.requireNonNull(assetManager.list(""))) {
                if (fileName.endsWith(".txt")) {
                    String quizName = fileName.replace(".txt", "");
                    if (preferences.getInt(quizName + "_COMPLETED_QUESTIONNAIRES", 0) == 0) {
                        Log.d("COMPLETE", "displayQuizzList: " + quizName);
                        quizNames.add(quizName);


                    }
                }
            }

            /* Ici, on va regarder les fichiers interne*/
            String[] internalFiles = fileList();
            for (String fileInterne : internalFiles) {
                if (fileInterne.endsWith(".txt") && preferences.getInt(fileInterne.replace(".txt", "")+ "_COMPLETED_QUESTIONNAIRES", 0) == 0) {
                    Log.d("INTERNE", "displayQuizzList: " + fileInterne);
                    quizNames.add(fileInterne.replace(".txt", ""));
                }
            }



            /* Adaptater pour mettre notre tableau dans un listView */
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, quizNames);

            ListView listView = findViewById(R.id.displayQuizz);
            listView.setAdapter(adapter);

            /* Ici, quand on clique sur un item du listView, on va dans le questionnaire en passant sont nom
            Pour ensuite pouvoir afficher les questions associées au quizz !
             */
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String selectedQuizName = quizNames.get(position);

                    Intent intent = new Intent(DisplayQuizzActivity.this, QuestionnaireActivity.class);
                    intent.putExtra("QUIZ_NAME", selectedQuizName);
                    startActivity(intent);
                }
            });

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
