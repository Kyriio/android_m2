package com.example.projetpestelle;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.icu.util.Output;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CreateQuizzActivity extends AppCompatActivity {

    /* délcaration des variables*/
    EditText categoryField;
    EditText questionField;
    EditText choix1;
    EditText choix2;
    EditText choix3;

    Button cancel_create_button;

    Button FinishButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_quizz);

        /* initialisation des variables*/
        categoryField = findViewById(R.id.category_field);
        questionField = findViewById(R.id.question_field);

        choix1 = findViewById(R.id.choix_1_field);
        choix2 = findViewById(R.id.choix_2_field);
        choix3 = findViewById(R.id.choix_3_field);

        cancel_create_button = findViewById(R.id.cancel_create_button);
        FinishButton = findViewById(R.id.FinishButton);


        /* si on cancel, en fait ça revient juste au main */
        cancel_create_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CreateQuizzActivity.this,"Merci pour votre visite !", Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(CreateQuizzActivity.this,MainActivity.class);
                startActivity(myIntent);
            }
        });

        /* si on veux créer / ajouter une question à un quizz ( sauvegarde interne ) */

        FinishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(FileExist(categoryField.getText().toString())){
                    Toast.makeText(CreateQuizzActivity.this,"La catégorie existe déjà ! ", Toast.LENGTH_SHORT).show();
                    addQuestion();
                }else{
                    Toast.makeText(CreateQuizzActivity.this,"Nouvelle catégorie ! ", Toast.LENGTH_SHORT).show();
                    saveQuestion();
                }
            }
        });
    }

    /* Ici, on va vérifier si la catégorie existe déjà
    * Il faut savoir que les assets sont en readOnly donc
    * On va faire en sorte de ne pas pouvoir rentrer la même catégorie pour des soucis d'affichage derrière
    * Et on regarde si dans les fichiers interne il y a un fichier avec le même nom
     */
    public Boolean FileExist(String categoryName){

        AssetManager assetManager = getAssets();
        try{
            /* on va chercher dans les assets*/
            String[] files = assetManager.list("");
            for (String file : files){
                if (file.toLowerCase().equals(categoryName.toLowerCase()+".txt")){
                    return true;
                }
            }

            /* on va chercher dans les fichiers interne*/
            String[] internalFiles = fileList();
            for (String fileInterne : internalFiles) {
                if (fileInterne.toLowerCase().equals(categoryName.toLowerCase()+".txt")) {
                    return true;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return false;
    }

    /* Ici, on va faire une fonction pour gérer le fait que la personne rentre une catégorie qui
    se trouve dans les assets (pour éviter d'avoir des problèmes
     */
    public boolean is_in_Assets(String category){

        AssetManager assetManager = getAssets();
        try{
            String[] questionnaireNames = getAssets().list("");
            List<String> myList = Arrays.asList(questionnaireNames);

            for (int i =0; i < questionnaireNames.length ;i ++){
                if (questionnaireNames[i].endsWith(".txt")){

                    questionnaireNames[i] = questionnaireNames[i].substring(0,questionnaireNames[i].length() -4);
                    Log.d("MyQuestions", "addQuestion: "+ questionnaireNames[i]);
                }
            }

            Log.d("CheckBoolean", "is_in_Assets: " + myList.contains(category));
            return myList.contains(category);

        }catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /* Ici donc, si la catégorie existe déjà on va ajouter la question à la catégorie */
    public void addQuestion() {
        /* on récupèe les valeurs des EditText*/
        String category = categoryField.getText().toString().trim();
        String question = questionField.getText().toString().trim();
        String choix_1 = choix1.getText().toString();
        String choix_2 = choix2.getText().toString();
        String choix_3 = choix3.getText().toString();

        int countedNumber =0;

        /* ajout on prends en compte que les réponses puissent aussi contenir des x et pas que la
         * bonne réponse !
         * exemple "choux boux roux"
         * */
        if(choix_1.split(" ").length < 2 && choix_2.split(" ").length < 2 && choix_3.split(" ").length < 2){
            Log.d("TEST_SPLIT", "saveQuestion: pas de réponses valide");
            Toast.makeText(CreateQuizzActivity.this,"Veuillez ajouter un espace puis un x à la réponse correcte", Toast.LENGTH_SHORT).show();
        }else {
            Log.d("BOOLEANTEST", "addQuestion: " + is_in_Assets(category) );
            /* On vérifie que tout est rempli */
            if (category.isEmpty() || question.isEmpty() || choix_1.isEmpty() || choix_2.isEmpty() || choix_3.isEmpty()) {
                Toast.makeText(CreateQuizzActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
            }else{
                /* On vérifie que la catégorie n'existe pas dans les assets */
                if (is_in_Assets(category)){
                    Toast.makeText(CreateQuizzActivity.this,"Veuillez choisir un autre nom de catégorie !", Toast.LENGTH_SHORT).show();
                }else{
                    /* On vérifie que la réponse à la question a bien été choisie */
                    if(!choix_1.endsWith(" x") && !choix_2.endsWith(" x") && !choix_3.endsWith(" x")){
                        Toast.makeText(CreateQuizzActivity.this,"Veuillez ajouter un x à la réponse correcte", Toast.LENGTH_SHORT).show();
                    }else{
                        /* ici on regarde si l'utilisateur essaie de mettre plusieurs réponses */
                        if(choix_1.endsWith(" x")) {
                            countedNumber +=1;
                        }
                        if(choix_2.endsWith(" x")) {
                            countedNumber += 1;
                        }
                        if(choix_3.endsWith(" x")) {
                            countedNumber+=1;
                        }

                        /* Si ce n'est pas le cas, on ajoute la question au questionnaire existant*/
                        if(countedNumber >1) {
                            Log.d("TEST_X", "saveQuestion: plus d'une bonne réponse");
                            Toast.makeText(CreateQuizzActivity.this,"Veuillez ne choisir qu'une seule réponse !", Toast.LENGTH_SHORT).show();
                        }else{
                            try {
                                /* On va écrire dans le fichier de la catégorie la question et les réponses */
                                FileOutputStream fileOutputStream = openFileOutput(category + ".txt", Context.MODE_APPEND);
                                fileOutputStream.write(("Q;" + question + "\n").getBytes());
                                fileOutputStream.write(("A;" + choix_1 + "\n").getBytes());
                                fileOutputStream.write(("A;" + choix_2 + "\n").getBytes());
                                fileOutputStream.write(("A;" + choix_3 + "\n").getBytes());
                                fileOutputStream.close();
                                Toast.makeText(CreateQuizzActivity.this,"Question ajoutée avec succès ! ", Toast.LENGTH_SHORT).show();
                                finish();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    /* Fonction pour ajouter une catégorie */
    public void saveQuestion(){


        /* Ici on prend les variables */
        int countedNumber =0;
        String category = categoryField.getText().toString().trim();
        String question = questionField.getText().toString().trim();
        String choix_1 = choix1.getText().toString();
        String choix_2 = choix2.getText().toString();
        String choix_3 = choix3.getText().toString();


        Log.d("TEST_SPLIT", "saveQuestion 1: " + choix_1.split(" ").length);
        Log.d("TEST_SPLIT", "saveQuestion 2: " + choix_2.split(" ").length);
        Log.d("TEST_SPLIT", "saveQuestion 3: " + choix_3.split(" ").length);

        /* ajout on prends en compte que les réponses puissent aussi contenir des x et pas que la
        * bonne réponse !
        * exemple "choux boux roux"
        * */
        if(choix_1.split(" ").length < 2 && choix_2.split(" ").length < 2 && choix_3.split(" ").length < 2){
            Log.d("TEST_SPLIT", "saveQuestion: pas de réponses valide");
            Toast.makeText(CreateQuizzActivity.this,"Veuillez ajouter un espace puis un x à la réponse correcte", Toast.LENGTH_SHORT).show();
        }else{
            /* On regarde si tout est remplis et si la réponse a été mise */
            if (category.isEmpty() || question.isEmpty() || choix_1.isEmpty() || choix_2.isEmpty() || choix_3.isEmpty()) {
                Toast.makeText(CreateQuizzActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
            }else {
                /* si aucune réponse finis pas un "x"*/
                if (!choix_1.endsWith(" x") && !choix_2.endsWith(" x") && !choix_3.endsWith(" x")) {
                    Toast.makeText(CreateQuizzActivity.this, "Veuillez ajouter un espace puis un x à la réponse correcte", Toast.LENGTH_SHORT).show();
                } else {

                    /* ici on regarde si l'utilisateur essaie de mettre plusieurs réponses */
                    if(choix_1.endsWith(" x")) {
                        countedNumber +=1;
                    }
                    if(choix_2.endsWith(" x")) {
                        countedNumber += 1;
                    }
                    if(choix_3.endsWith(" x")) {
                        countedNumber+=1;
                    }

                    /* si ce n'est pas le cas, on ajoute la question*/
                    if(countedNumber >1) {
                        Log.d("TEST_X", "saveQuestion: plus d'une bonne réponse");
                        Toast.makeText(CreateQuizzActivity.this,"Veuillez ne choisir qu'une seule réponse !", Toast.LENGTH_SHORT).show();
                    } else{
                        try {
                            /* ici beaucoup plus simple on va juste créer le nouveau fichier */
                            FileOutputStream fileOutputStream = openFileOutput(category + ".txt", Context.MODE_PRIVATE);
                            fileOutputStream.write(("Q;" + question + "\n").getBytes());
                            fileOutputStream.write(("A;" + choix_1 + "\n").getBytes());
                            fileOutputStream.write(("A;" + choix_2 + "\n").getBytes());
                            fileOutputStream.write(("A;" + choix_3 + "\n").getBytes());
                            fileOutputStream.close();

                            Toast.makeText(CreateQuizzActivity.this, "Questionnaire créé avec succès ! ", Toast.LENGTH_SHORT).show();
                            finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
