package com.example.exo_2intent02;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        TextView myText = (TextView) findViewById(R.id.TextFromFirst);


        Intent intent = getIntent();
        String init = intent.getStringExtra("initialText");
        myText.setText(init);


    }

    public void Majtext(View view){

        TextView myText = (TextView) findViewById(R.id.TextFromFirst);

        String upper = myText.getText().toString().toUpperCase();
        Intent resultIntent = new Intent();
        resultIntent.putExtra("modifiedText", upper);

        setResult(RESULT_OK, resultIntent);

        finish();
    }

    public void ReverseText(View view){

        TextView myText = (TextView) findViewById(R.id.TextFromFirst);
        String reverse = new StringBuilder(myText.getText().toString()).reverse().toString();
        Intent resultIntent = new Intent();
        resultIntent.putExtra("modifiedText", reverse);

        setResult(RESULT_OK, resultIntent);

        finish();
    }

}