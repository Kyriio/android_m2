package com.example.exo_2intent02;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ActivityResultLauncher<Intent> activityLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activityLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {
                            String modifiedText = data.getStringExtra("modifiedText");
                            TextView DisplayModif = findViewById(R.id.ModifiedText);
                            DisplayModif.setText(modifiedText);
                        }
                    }
                }
        );

    }

    public void SendForModif(View view) {
        Intent intent = new Intent(this, SecondActivity.class);
        String currentText = ((TextView) findViewById(R.id.TextToModif)).getText().toString();
        intent.putExtra("initialText", currentText);
        activityLauncher.launch(intent);

    }

    public void ValidateModif(View view){
        TextView ModifField = (TextView) findViewById(R.id.ModifiedText);
        String modified =  ModifField.getText().toString();

        EditText toModif = (EditText) findViewById(R.id.TextToModif);

        toModif.setText(modified);

    }
}