package com.example.contentproviderpestelle;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private SwitchCompat switchButton;
    private ListView wordListView;
    private ArrayList<String> myListWord;
    private Button delButton;
    private Button addButton;
    private EditText myWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialisation des boutons
        switchButton = findViewById(R.id.switchAlpha);
        wordListView = findViewById(R.id.wordListView);
        delButton = findViewById(R.id.removeButton);
        addButton = findViewById(R.id.addButton);
        myWord = findViewById(R.id.editTextWord);

        myListWord = new ArrayList<>();

        //Appel de l'initialisation de l'application
        initialize();

        //Si on change le bouton on change l'ordre alphabétique
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateSort();
            }
        });

        //Si on appuie sur le bouton supprimer, on supprimel e mot
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wordToDel = myWord.getText().toString();
                if(myListWord.contains(wordToDel)){
                    deleteWord(wordToDel);
                } else {
                    Log.d("NOWORD", "deleteWord: Le mot n'existe pas dans la liste");
                }
            }
        });

        //Si on appuie sur le bouton supprimer, on ajoute le mot
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wordToAdd = myWord.getText().toString();
                if (!myListWord.contains(wordToAdd)) {
                    addWord(wordToAdd);
                } else {
                    Log.d("WORDIN", "addWord: Le mot est déjà dans la liste");
                }
                //Ici seul moment où je rappel initialize vu qu'en fait je l'ajoute et du coup il ne peux pas afficher correctement le changement
                initialize();
            }
        });
    }

    //Ici, fonction pour ajouter dans le dictionnaire le mot de l'utilisateur
    private void addWord(String word) {
        ContentValues values = new ContentValues();
        values.put(UserDictionary.Words.APP_ID, getApplicationContext().getPackageName());
        values.put(UserDictionary.Words.LOCALE, getResources().getConfiguration().locale.toString());
        values.put(UserDictionary.Words.WORD, word);
        values.put(UserDictionary.Words.FREQUENCY, 250);
        getContentResolver().insert(UserDictionary.Words.CONTENT_URI, values);
    }

    //Ici, on delete le mot de l'utilisateur
    private void deleteWord(String word) {
        myListWord.remove(word);
        updateList();
        getContentResolver().delete(UserDictionary.Words.CONTENT_URI, UserDictionary.Words.WORD + "=?", new String[]{word});
    }

    //Update de l'affichage de la liste
    private void updateSort() {
        String sort;

        //On regarde si le bouton est check ou non
        if (switchButton.isChecked()) {
            sort = UserDictionary.Words.WORD + " COLLATE NOCASE ASC";
        } else {
            sort = UserDictionary.Words.WORD + " COLLATE NOCASE DESC";
        }

        //appel avec un curseur en fonction de s'il est ou non checked
        Cursor cursor = getContentResolver().query(UserDictionary.Words.CONTENT_URI, null, null, null, sort);

        //ici, on va clear + aller chercher les mots et update l'affichage
        if (cursor != null) {
            myListWord.clear();
            while (cursor.moveToNext()) {
                String word = cursor.getString(cursor.getColumnIndex(UserDictionary.Words.WORD));
                myListWord.add(word);
            }
            cursor.close();
            updateList();
        }
    }

    //Fonction pour qui permet d'update la liste ( car trop récurrente )
    private void updateList() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, myListWord);
        wordListView.setAdapter(adapter);
    }

    //Fonction d'initialisation pour aller de base mettre tous les mots du dictionnaire dans l'appli au démarrage
    private void initialize() {
        Cursor cursor = getContentResolver().query(UserDictionary.Words.CONTENT_URI, null, null, null, UserDictionary.Words.WORD + " COLLATE NOCASE DESC");

        if (cursor != null) {
            myListWord.clear();
            while (cursor.moveToNext()) {
                String word = cursor.getString(cursor.getColumnIndex(UserDictionary.Words.WORD));
                myListWord.add(word);
            }
            cursor.close();
            updateList();
        }
        updateSort();
    }

}
